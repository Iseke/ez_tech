from rest_framework import generics

from api.models import Areas
from api.serializers import AreasSerializer


class AreasView(generics.ListCreateAPIView):
    serializer_class = AreasSerializer
    queryset = Areas.objects.all()