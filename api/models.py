from django.db import models
from django.contrib.postgres.fields import ArrayField


class Areas(models.Model):
    name = models.CharField(max_length=155)
    description = models.CharField(max_length=255)
    points = ArrayField(models.FloatField())
    fill_type = models.CharField(max_length=55)
    color = ArrayField(models.CharField(max_length=50, blank=True))
    angle = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f'{self.name}  -  {self.description}'
