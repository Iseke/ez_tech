from django.urls import path

from api import views

urlpatterns = [
    path('areas/', views.AreasView.as_view())
]