from rest_framework import serializers

from api.models import Areas


class AreasSerializer(serializers.ModelSerializer):
    fields_to_be_removed = ['value']

    class Meta:
        model = Areas
        fields = ['id', 'name', 'description', 'points', 'fill_type',
                  'color', 'angle']

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        for i in rep:
            try:
                if rep[i] is None:
                    rep.pop(i)
            except KeyError:
                pass
        return rep
